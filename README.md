###Libraries

The stylesheet inspector requires:

- cssbeautify (https://github.com/senchalabs/cssbeautify)
- code-prettify (https://github.com/google/code-prettify)

They should be installed in `/libraries/cssbeautify` and 
`/libraries/code-prettify` respectively.